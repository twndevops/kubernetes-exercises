FROM amazoncorretto:17.0.9-al2023-headful

# ENV vars here are default values overwritten by Java app deploy manifest env var values
# ENV DB_SERVER=mysql-primary.default.svc.cluster.local \
#     DB_NAME=bXlzcWxkYg== \
#     DB_USER=bm9ucm9vdA== \
#     DB_PWD=bm9ucm9vdHB3ZA==

RUN mkdir -p app
COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /app
WORKDIR app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]